<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>project mysql</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="project.css">
</head>
<body>
<header>
	<a href="../../projectP.html"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	<h1> Project mysql </h1>
</header>
<main>
	<table cellspacing="0" cellpadding="0">
		<caption> liste des jeux</caption>
		<thead>
			<tr>
				<th>Numéros</th>
				<th>Nom du jeux</th>
				<th>Style</th>
				<th>prix</th>
			</tr>
		</thead>
		<tbody>

			<?php foreach ($orders as $order):?>

			<tr>
				<td><?= $order['NumerosJeux']?></td>
				<td><?= $order['NomJeux']?></td>
				<td><?= $order['Style']?></td>
				<td><?= $order['Prix']?></td>
				<td><button><a href="jeuxSelect.php?NumerosJeux=<?= $order['NumerosJeux']?>">
					<i class="fa fa-check-square" aria-hidden="true">
				</a></i></button></td>
			</tr>

			<?php endforeach ?>

		</tbody>
	</table>
</main>
</body>
</html>