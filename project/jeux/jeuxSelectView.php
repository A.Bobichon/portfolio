<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>project mysql2</title>
	<link rel="stylesheet" type="text/css" href="project.css">
</head>
<body>
<header>
	<h1> Project mysql </h1>
	<a href="jeux.php"> Retour a l'accueil</a>
</header>
<main>
	<table cellspacing="0" cellpadding="0">
		<caption> liste des jeux</caption>
		<thead>
			<tr>
				<th>Numéros</th>
				<th>Nom du jeux</th>
				<th>Nombre joueur</th>
				<th>Console</th>
				<th>Limite d'age</th>
			</tr>
		</thead>
		<tbody>

			<tr>
				<td><?= $games['NumerosJeux']?></td>
				<td><?= $games['NomJeux']?></td>
				<td><?= $games['NombreJoueur']?></td>
				<td><?= $games['ConsoleType']?></td>
				<td><?= $games['LimiteAge']?></td>
			</tr>
  			
		</tbody>
	</table>
</main>
</body>
</html>