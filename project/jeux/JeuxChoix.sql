-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 12 jan. 2018 à 18:03
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `jeux`
--

-- --------------------------------------------------------

--
-- Structure de la table `JeuxChoix`
--

DROP TABLE IF EXISTS `JeuxChoix`;
CREATE TABLE IF NOT EXISTS `jeuxchoix` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NomJeux` varchar(40) NOT NULL,
  `Style` varchar(40) NOT NULL,
  `Prix` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `JeuxChoix`
--

INSERT INTO `JeuxChoix` (`Id`, `NomJeux`, `Style`, `Prix`) VALUES
(1, 'Mario sunshine', 'Plateform', 40),
(2, 'Dragon ball tenkachi', 'Combat', 25),
(3, 'League of legend', 'MOBA', 0),
(4, 'World of warcraft', 'MMORPG', 45),
(5, 'Skate3', 'Sport', 65),
(6, 'Mario sunshine', 'Plateform', 40),
(7, 'Dragon ball tenkachi', 'Combat', 25),
(8, 'League of legend', 'MOBA', 0),
(9, 'World of warcraft', 'MMORPG', 45),
(10, 'Skate3', 'Sport', 65);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
