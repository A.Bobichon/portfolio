let index = 0;

//on prend la save du joueur//

let joueurCombat = localStorage.getItem('joueurSave');
joueurCombat = JSON.parse(joueurCombat);

console.log(joueurCombat);

//declare l'ennemi
let maxLife = 100;

let ennemi = new Ennemi('ennemi', 10, 100, 10, 10);

let ennemiImage = document.querySelector('#ennemi');

let vieAffEnnemi = document.querySelector('.progressViE');
vieAffEnnemi.textContent = ennemi.vie;


// execute le combat
ennemiImage.addEventListener('click', function () {
   

  //affiche les degat subis du monstre
  let dommage = ennemi.vie -= joueurCombat.attaque;

  let pourcen = (ennemi.vie*100) /maxLife;
  console.log(Math.round(pourcen));

  /////////////calcul////////////////////

  console.log(ennemi.vie);
  vieAffEnnemi = document.querySelector('.progressViE');
  vieAffEnnemi.style.width =  Math.round(pourcen) + '%';
  vieAffEnnemi.textContent = dommage;

  //affiche le niveau
  let niveau = document.querySelector('.niveau');

  niveau.textContent = 'niv:' + joueurCombat.level;



  // affiche le niveau en temps réele


  // le combat tourne en boucle
  if (ennemi.vie <= 0) {
    console.log('tu as gagner le combat');
    // le son du monstre
    let son = new Audio('../mp3/0477.mp3');
    son.play();

    if (ennemi.vie <= 0) {
      console.log('tu as gagner ' + ennemi.gold + ' gold');

      //faire le changement d'ennemi 

      let imagEnnemi = document.querySelector('#ennemi');
      let tabImg = ['../img/2a7df96fff052ba194619f76915a60cc.png', '../img/3df31754e07ecc707eb0701c7a80bc86.png', '../img/12-2-zombie-png-picture.png', '../img/49fd3a6243a78bdc6d46c0c70ab106e0.png', '../img/3048563cf380929d9f1a67936a52d390.png', '../img/69399732fb18531d222b93cff2f27599.png', '../img/2040016000.png', '../img/Alghul.png', '../img/b851941f28a5883cb594f91da7f3cd007505fa62.png', '../img/Cerberus.png', '../img/FFBE_Fenrir_Artwork_2.png', '../img/Mothman_Artist\'s_Impression.png', '../img/wendigo.png', '../img/', '../img/Wraith.png',]


      index++;

      if (index === 13) {
        index = 0;

      }

      let imgaff = tabImg[index];

      imagEnnemi.src = imgaff;

      console.log(index);




    }
    maxLife = 100 + joueurCombat.level * 20;
    ennemi = new Ennemi('ennemi', 10, maxLife, 10, 10 + joueurCombat.level * 10); // faire en sorte que les sprites changes
    vieAffEnnemi.style.width = '100%';
    vieAffEnnemi.textContent = maxLife;

    //le joueur gagne de l'xp
    joueurCombat.xp += ennemi.xp;


    //le joueur passe un niveaux
    if (joueurCombat.xp === 100) {
      console.log('tu gagne un niveau')
    }

    //on gagne des gold
    joueurCombat.gold += ennemi.gold;

    console.log(joueurCombat);

    //augmentation de niveau
    if (joueurCombat.xp >= joueurCombat.needXp) {
      joueurCombat.needXp += 100;
      joueurCombat.level += 1;
      joueurCombat.xp = 0;
      console.log('tu passe un niveau');
    }
  }

  //afficher le somme de gold

  let gold = document.querySelector('.progressGold');

  gold.textContent = joueurCombat.gold + ' gold';

  let attaque = document.querySelector('.progressAttaque');

  attaque.textContent = joueurCombat.attaque + ' attaque'

  //affiche xp

  let xpProgress = document.querySelector('.progressXp');
  xpProgress.textContent = joueurCombat.xp + '/' + joueurCombat.needXp;
  let pourcentage = joueurCombat.xp * 100 / joueurCombat.needXp;
  xpProgress.style.width = pourcentage + '%';
});



//on revien sur la page de jeu et on enregistre les données

let buttonRetour = document.querySelector('.retour');

buttonRetour.addEventListener('click', function () {
  localStorage.setItem('joueurSave', JSON.stringify(joueurCombat));

});

//affiche xp

let xpProgress = document.querySelector('.progressXp');
xpProgress.textContent = joueurCombat.xp + '/' + joueurCombat.needXp;


