/*la base des personnages */

class Joueur{
  /*stats en commun joueur, monstre*/
  constructor(nom,attaque,vie,xp,bonneur,gold,fain,soif,genre,level,needXp,img){
    this.nom = nom;
    this.attaque = attaque;
    this.vie = vie;
    this.xp = xp;
    this.bonneur = bonneur;
    this.gold = gold;
    this.fain = fain;
    this.soif = soif;
    this.genre = genre;
    this.level = level;
    this.needXp = needXp;
    this.img = img;
  }

}

