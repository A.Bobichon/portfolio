'use strict';  

// Codes des touches du clavier.
const TOUCHE_1 = 97;
const TOUCHE_2 = 98;
const TOUCHE_3 = 99;
const TOUCHE_4 = 100;
const TOUCHE_5 = 101;
const TOUCHE_ESPACE = 32;
const TOUCHE_GAUCHE = 37;
const TOUCHE_DROITE = 39;


// La liste des slides du carrousel.
var slides =
[
    { image: 'images/1.jpg', legend: 'Renard'},
    { image: 'images/2.jpg', legend: 'hérisson'},
    { image: 'images/3.jpg', legend: 'furet'},
    { image: 'images/4.jpg', legend: 'singe'},
    { image: 'images/5.jpg', legend: 'girafes'},
];

// Objet contenant l'état du carrousel.
var state;

function onSliderGoToNext()
{
    // Passage à la slide suivante.
    state.index++;

    // Est-ce qu'on est arrivé à la fin de la liste des slides ?
    if(state.index == slides.length)
    {
        // Oui, on revient au début (le carrousel est circulaire).
        state.index = 0;
    }

    // Mise à jour de l'affichage.
    refreshSlider();
}

function onSliderGoToPrevious()
{
    // Passage à la slide précédente.
    state.index--;

    // Est-ce qu'on est revenu au début de la liste des slides ?
    if(state.index < 0)
    {
        // Oui, on revient à la fin (le carrousel est circulaire).
        state.index = slides.length - 1;
    }

    // Mise à jour de l'affichage.
    refreshSlider();
}

function onSliderGoToRandom()
{
    var index;

    do
    {
        index = getRandomInteger(0, slides.length - 1);
    }
    while(index == state.index);

    // Passage à une slide aléatoire.
    state.index = index;

    // Mise à jour de l'affichage.
    refreshSlider();
}


function onSliderKeyUp(event)
{
   

    switch(event.keyCode)
    {
        case TOUCHE_DROITE:
        onSliderGoToNext();
        break;

        case TOUCHE_ESPACE:
        onSliderToggle();
        break;

        case TOUCHE_GAUCHE:
        onSliderGoToPrevious();
        break;

        case TOUCHE_1:
        sliderImg();
        break;

        case TOUCHE_2:
        sliderImg2();
        break;

        case TOUCHE_3:
        sliderImg3();
        break;

        case TOUCHE_4:
        sliderImg4();
        break;

        case TOUCHE_5:
        sliderImg5();
        break;


    }
}

function onSliderToggle()
{
    var icon;

    // Modification de l'icône du bouton pour démarrer ou arrêter le carrousel.
    icon = document.querySelector('#slider-toggle i');

    icon.classList.toggle('fa-play');
    icon.classList.toggle('fa-pause');

    // Est-ce que le carousel est démarré ?
    if(state.timer == null)
    {
        // Non, démarrage du carousel, toutes les deux secondes.
        state.timer = window.setInterval(onSliderGoToNext, 2000);

       
        this.title = 'Arrêter le carrousel';
    }
    else
    {
        // Oui, arrêt du carousel.
        window.clearInterval(state.timer);

        // Réinitialisation de la propriété pour le prochain clic sur le bouton.
        state.timer = null;

        this.title = 'Démarrer le carrousel';
    }
}

function onToolbarToggle()
{
    var icon;

    // Modification de l'icône du lien pour afficher ou cacher la barre d'outils.
    icon = document.querySelector('#toolbar-toggle i');

    icon.classList.toggle('fa-arrow-down');
    icon.classList.toggle('fa-arrow-right');

    // Affiche ou cache la barre d'outils.
    document.querySelector('.toolbar ul').classList.toggle('hide');
}

function refreshSlider()
{
    var sliderImage;
    var sliderLegend;

    // Recherche des balises de contenu du carrousel.
    sliderImage  = document.querySelector('#slider img');
    sliderLegend = document.querySelector('#slider figcaption');

    // Changement de la source de l'image et du texte de la légende du carrousel.
    sliderImage.src          = slides[state.index].image;
    sliderLegend.textContent = slides[state.index].legend;
}


function sliderImg(){

   state.index = 0;

   refreshSlider();
}

function sliderImg2(){

   state.index = 1;

   refreshSlider();
}

function sliderImg3(){

   state.index = 2;

   refreshSlider();
}

function sliderImg4(){

   state.index = 3;

   refreshSlider();
}

function sliderImg5(){

   state.index = 4;

   refreshSlider();
}

document.addEventListener('DOMContentLoaded', function()
{
    // Initialisation du carrousel.
    state       = {};
    state.index = 0;                  
    state.timer = null; 
                
    // Installation des gestionnaires d'évènement.
    installEventHandler('#slider-random', 'click', onSliderGoToRandom);
    installEventHandler('#slider-previous', 'click', onSliderGoToPrevious);
    installEventHandler('#slider-next', 'click', onSliderGoToNext);
    installEventHandler('#slider-toggle', 'click', onSliderToggle);
    installEventHandler('#toolbar-toggle', 'click', onToolbarToggle);
    installEventHandler('#one', 'click', sliderImg);
    installEventHandler('#two', 'click', sliderImg2);
    installEventHandler('#three', 'click', sliderImg3);
    installEventHandler('#four', 'click', sliderImg4);
    installEventHandler('#five', 'click', sliderImg5);
   

    document.addEventListener('keyup', onSliderKeyUp);
   
    refreshSlider();
});