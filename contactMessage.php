<?php

$errors = [];
$prenom = htmlspecialchars($_POST['prenom']);
$nom = htmlspecialchars($_POST['nom']);
$mail = $_POST['mail'];
$message = htmlspecialchars($_POST['message']);

if (!array_key_exists('nom',$_POST) || $nom == '') {
	$errors['nom'] = "Vous n'avez pas renseignier votre nom !!";
}
if (!array_key_exists('prenom',$_POST) || $prenom == '') {
	$errors['prenom'] = "Vous n'avez pas renseignier votre prenom !!";
}
if (!array_key_exists('mail',$_POST) || $mail == '' || filter_var($mail, FILTER_VALIDATE_EMAIL)) {
	$errors['mail'] = "Vous n'avez pas renseignier votre mail !!";
}
if (!array_key_exists('message',$_POST) || $message == '') {
	$errors['message'] = "Vous n'avez pas renseignier votre message!!";
}

session_start();

if (!empty($errors)) {
	$_SESSION['errors'] = $errors;
	$_SESSION['inputs'] = $_POST;
	header('location: contact.php');
} else {
	$message;
	$_SESSION['succes'] = 1;
	$headers = 'FROM: site@local.dev';
	mail('alexandrebobichon@gmail.com', 'Formulaire de contact', $message ,$headers);
	header('location: contact.php');
}
