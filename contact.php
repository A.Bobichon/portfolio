<?php session_start();?>
<!DOCTYPE html>
<html lang="fr">
<meta charset="utf-8">
<head>
	<title>portfolio</title>
	<link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah|Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Spectral+SC" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!-- jQuery -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
<header>
	<nav>
		<ul class="menu-demo2">
			<li id="home" class="menu">
			<a href="index.html"><span>home</span></a>
			</li>

			<li id="project" class="menu">
			<a href="projectP.html"><span>project</span></a>
			</li>

			<li id='presentation' class="menu">
			<a href="presentation.html"><span>presentation</span></a>
			</li>

			<li id='contact' class="menu">
			<a href="contact.php"><span>contact</span></a>
			</li>
		</ul>
	</nav>

</header>

<main id="target">

<section class="stylecontact">
<h1>contact</h1>


<div id="container"> 
	<h2>me-contactez</h2>
	

	<fieldset>

		<?php 
		if (array_key_exists('errors', $_SESSION)):?> 
			
			<div class="alert"> 
				<?= implode('<br>', $_SESSION['errors']); ?>
			</div>

		<?php endif; ?>

		<?php 
		if (array_key_exists('succes', $_SESSION)):?> 
			
			<div class="succes"> 
				<p> votre email est bien pris en compte.</p>
			</div>

		<?php endif; ?>

		<form id="form" method="POST" action="contactMessage.php">
		<ul>
			<li>
				<label for="nom">nom:</label>
				<input type="text" name="nom" value="<?= isset($_SESSION['inputs']['nom']) ? $_SESSION['inputs']['nom'] : '';?>">

				<label for="prenom">prénom:</label>
				<input type="text" name="prenom" value="<?= isset($_SESSION['inputs']['prenom']) ? $_SESSION['inputs']['prenom'] : '';?>">
			</li>
			<li>
				<label for="mail"> email</label>
				<input type="email" name="mail" value="<?= isset($_SESSION['inputs']['mail']) ? $_SESSION['inputs']['mail'] : '';?>">
			</li>
		
			<li>
				<label for="message"> votre message </label>
				<textarea name="message" value="<?= isset($_SESSION['inputs']['message']) ? $_SESSION['inputs']['message'] : '';?>"></textarea>
			</li>

			<li>
				<button type="submit" name='mailform'>envoyer</button>
				<button type="reset"> supprimer</button>
			</li>
			
		</ul>
		</form>
		
	</fieldset>

</section>

</main>

<footer>
	<h3>réseaux sociaux:</h3>

	<nav>
	<a href="hrefhttps://www.linkedin.com/in/alexandre-bobichon-648291b9/"><p><i class="fa fa-linkedin-square" aria-hidden="true"></i> linkedin</p></a>
		<a href="#"><p><i class="fa fa-phone-square" aria-hidden="true"></i>06 47 57 00 48 </p></a>
		<a href="#"><p><i class="fa fa-envelope" aria-hidden="true"></i> alexandrebobichon@gmail.com</p></a>
	</nav>

</footer>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>

<?php
unset($_SESSION['inputs']);
unset($_SESSION['errors']);
unset($_SESSION['succes']);
?>