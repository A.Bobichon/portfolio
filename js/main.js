
//carousel home

var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 3000); 
};

//presentation cercle

jQuery(document).ready(function($) {


	$('.button-info').on('click', function(){
		$.get('php/presentation.php',function(data){
		 		$("#target").html(data);
		 	})
	})

	});


jQuery(document).ready(function($) {

function cercleBase(canvas){
    var canvas = document.getElementById(canvas); 
    var context = canvas.getContext("2d");
    context.beginPath();
    context.lineWidth="5";
    context.strokeStyle = 'rgb(71, 153, 233)';
    context.arc(125,125, 90, 0, 2 * Math.PI);
    context.stroke();
}

function cerclePourcentage(canvas ,X){
	var canvas = document.getElementById(canvas); 
    var context = canvas.getContext("2d");
	context.beginPath();
	context.arc(125, 125, 87.65, 0, X/Math.PI);
	context.lineTo(125,125);
	context.fillStyle = 'rgb(128, 191, 252)';
	context.fill();
}

cercleBase('canvas');
cercleBase('canvas1');
cercleBase('canvas2');
cercleBase('canvas3');

cerclePourcentage('canvas',18);
cerclePourcentage('canvas1',16);
cerclePourcentage('canvas2',14);
cerclePourcentage('canvas3',14.8);

});

 